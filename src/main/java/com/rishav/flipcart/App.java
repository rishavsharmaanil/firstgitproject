package com.rishav.flipcart;

import com.jsp.EntityClass.Cart;
import com.jsp.EntityClass.Item;
import com.jsp.Repository.CartRepository;
import com.jsp.Repository.ItemRepository;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		System.out.println("Hello World!");
//        Cart cart = new Cart();
//        cart.setId(101L);
//        cart.setItemId(1001L);
//        cart.setModeOfPayMent("Cash");
//        cart.setQuantity(5L);
//        cart.setTotalPrice(1000L);
//        cart.setCreateDate("22/11/22");
//        
		CartRepository cartRepository = new CartRepository();
		// cartRepository.saveCart(cart);

		Item item = new Item();
		item.setId(001);
		item.setItemId(100L);
		item.setPrice(5000L);
		item.setTotalStack(155L);
		item.setCreatedDate("22-11-22");

		ItemRepository repository = new ItemRepository();
		// repository.saveItem(item);

		// repository.updateTotalStock(10L,1L );
		System.out.println(repository.findAll());

	}

}

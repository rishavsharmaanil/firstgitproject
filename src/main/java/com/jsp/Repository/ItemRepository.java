package com.jsp.Repository;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.jsp.EntityClass.Item;
import com.jsp.Util.SessionFactoryUtil;

public class ItemRepository {

	public List<Item> findAll() {

		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		Query query = session.createQuery("from Item");
		return query.getResultList();

	}

	public void saveItem(Item item) {
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.save(item);
		transaction.commit();
	}

	public Item getItemById(long l) {
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		Item item = session.get(Item.class, l);

		return item;

	}

	public void updateTotalStock(Long numberStock, Long id) {

		Item item = getItemById(id);
		item.setTotalStack(item.getTotalStack() + numberStock);
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		Transaction beginTransaction = session.beginTransaction();
		session.update(item);
		beginTransaction.commit();
		session.close();
	}

}

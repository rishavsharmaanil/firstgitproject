package com.jsp.Repository;

import com.jsp.DtoClass.DtoCart;
import com.jsp.EntityClass.Cart;

public class CardDetails {

	public void sell(DtoCart dc) {

		Cart cart = new Cart();
		cart.setItemId(dc.getId());
		cart.setQuantity(dc.getQuantity());
		cart.setModeOfPayMent(dc.getModeOfPayment());

		ItemRepository itemRepository = new ItemRepository();
		long price = itemRepository.getItemById(dc.getId()).getPrice();
		cart.setTotalPrice(price * dc.getQuantity());
		cart.setQuantity(dc.getQuantity());
		CartRepository cr = new CartRepository();
		cr.saveCart(cart);

	}

}

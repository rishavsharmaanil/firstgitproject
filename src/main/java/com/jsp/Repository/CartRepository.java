package com.jsp.Repository;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.jsp.EntityClass.Cart;
import com.jsp.EntityClass.Item;
import com.jsp.Util.SessionFactoryUtil;
import com.mysql.cj.xdevapi.SessionFactory;

public class CartRepository {

	public void saveCart(Cart cart) {
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.save(cart);
		transaction.commit();

	}

}

package com.jsp.EntityClass;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.jsp.Constant.AppConstant;

@Entity
@Table(name = AppConstant.shoppingCart_info)

public class Cart implements Serializable {
	@Id
	@GeneratedValue
	@Column(name ="id")
	private Long id;
	
	@Column(name ="itemId")
	private Long itemId;
	
	@Column(name ="quantity")
	private Long quantity;
	
	@Column(name ="totalPrice")
	private Long totalPrice;
	
	@Column(name ="modeOfPayMent")
	private String modeOfPayMent;
	
	@Column(name ="createDate")
	private String createDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(long d) {
		this.quantity = d;
	}

	public Long getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Long totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getModeOfPayMent() {
		return modeOfPayMent;
	}

	public void setModeOfPayMent(String modeOfPayMent) {
		this.modeOfPayMent = modeOfPayMent;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	@Override
	public String toString() {
		return "Cart [id=" + id + ", itemId=" + itemId + ", quantity=" + quantity + ", totalPrice=" + totalPrice
				+ ", modeOfPayMent=" + modeOfPayMent + ", createDate=" + createDate + "]";
	}
	
		
	

}

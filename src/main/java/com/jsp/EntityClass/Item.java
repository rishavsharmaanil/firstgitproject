package com.jsp.EntityClass;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.jsp.Constant.AppConstant;

@Entity
@Table(name = AppConstant.shoppingItem_info)
public class Item implements Serializable{
	@Id
	@Column(name = "id")
	private Long id;
	
	@Column(name = "itemId")
	private Long itemId;
	
	@Column(name = "price")
	private Long price;
	
	@Column(name = "totalStack")
	private Long totalStack;
	
	@Column(name = "cretaedDate")
	private String createdDate;
	
	@Override
	public String toString() {
		return "Item [id=" + id + ", itemId=" + itemId + ", price=" + price + ", totalStack=" + totalStack
				+ ", createdDate=" + createdDate + "]";
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getItemId() {
		return itemId;
	}
	public void setItemId(long itemId) {
		this.itemId = itemId;
	}
	public long getPrice() {
		return price;
	}
	public void setPrice(long price) {
		this.price = price;
	}
	public long getTotalStack() {
		return totalStack;
	}
	public void setTotalStack(long totalStack) {
		this.totalStack = totalStack;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	
		
	

}

package com.jsp.Util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionFactoryUtil {
	
	public static SessionFactory sessionFactory;
	  
	private SessionFactoryUtil() {
	
	}
	public static SessionFactory getSessionFactory(){
		if(sessionFactory==null) {
			sessionFactory=new Configuration()
					.configure().buildSessionFactory();
		}
		
		return sessionFactory;
		
	}

}
